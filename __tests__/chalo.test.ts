import CHALO from "../src/chalo";
import { valid, noSelf } from "./__fixtures__/fixtures";
describe("CHALO class", function () {
  test("empty CHALO instantiable", () => {
    expect(new CHALO({}));
  });

  test("partial CHALO instantiable", () => {
    expect(new CHALO(noSelf));
  });

  test("CHALO instantiable", () => {
    expect(new CHALO(valid));
  });
});
